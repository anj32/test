package test;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import test1.StringHelper;

@RunWith(Parameterized.class)
public class StringHelperParameterizedTest {
	StringHelper helper = new StringHelper();
	
	//member variables
	private String input;
	private String expectedOutput;
	
	
	//constructor generated
	public StringHelperParameterizedTest(String input, String expectedOutput) {
		super();
		this.input = input;
		this.expectedOutput = expectedOutput;
	}
	@Parameters
	public static Collection<String[]> testCOnditions() {
		String expectedOuptts[][]={{"AACD","CD"},{"ACD","CD"},{"CDFG","CDFG"},{"CDAF","CDAF"},{"CDAA","CDAA"}};
		return Arrays.asList(expectedOuptts);
	}
	@Test
	public final void testTruncateAInFirst2Positions() {
		//expected , actual
		assertEquals(expectedOutput, helper.truncateAInFirst2Positions(input)); 	
	}
	
	
}
